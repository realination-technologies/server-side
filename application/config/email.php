<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
    'mailtype' => 'html',
    'charset' => 'iso-8859-1',
    'wordwrap' => TRUE
);