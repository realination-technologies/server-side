<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['(:any)']['OPTIONS'] = 'welcome/index_options';
$route['(:any)/(:any)']['OPTIONS'] = 'welcome/index_options';



$route['auth/login'] = 'AuthController/login';
$route['auth/hash'] = 'AuthController/hashPassword';


$route['speech']['GET'] = 'SpeechesController/listSpeeches';
$route['speech/search']['GET'] = 'SpeechesController/searchSpeech';
$route['speech']['POST'] = 'SpeechesController/insertSpeech';
$route['speech/(:any)']['PUT'] = 'SpeechesController/updateSpeech/$1';


$route['share']['POST'] = 'SpeechesController/shareSpeech';

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
