<?php
/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 1/5/2019
 * Time: 4:40 PM
 */

class SpeechModel extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table ='speeches';
    }


    public function getAllSpeeches(){
        return $this->get_all();
    }

    public function addSpeech($speech){
        return $this->insert($speech);
    }

    public function findById($id){
        $where = array(
            "id"=> $id
        );
        return $this->get($where);
    }

    public function editSpeech($id, $speech){
        $where = array(
            "id" => $id
        );
        return $this->update($speech, $where);
    }

    public function findSpeech($author, $subject, $date){

        if($author != ''){
            $this->db->like('author', $author);
        }
        if($subject != ''){
            $this->db->like('subject', $subject);
        }
        if($date != ''){
            $this->db->where('speech_date', $date);
        }

        $query = $this->db->get($this->table);

        return $query->result();
    }

}