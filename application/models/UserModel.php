<?php
/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 1/7/2019
 * Time: 3:50 AM
 */

class UserModel extends MY_Model {

    public function __construct(){
        parent::__construct();
        $this->table ='users';
    }

    public function checkLogin($username, $password){
        $where = array(
            'username' => $username
        );
        return $this->get($where);

    }

}