<?php
/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 1/5/2019
 * Time: 4:42 PM
 */

class SpeechesController extends MY_Controller {

    public function listSpeeches(){
        if($this->getBearerToken()) {
            echo json_encode($this->speechModel->getAllSpeeches());
        }else{
            echo $this->unauthorizedResponse();
        }
    }

    public function insertSpeech(){
        if($this->getBearerToken()) {
            $data = file_get_contents("php://input");
            $speech = json_decode($data, TRUE);
            echo json_encode($this->speechModel->addSpeech($speech));
        }else{
            echo $this->unauthorizedResponse();
        }
    }

    public function shareSpeech(){
        if($this->getBearerToken()) {
            $data = file_get_contents("php://input");
            $data = json_decode($data, TRUE);

            $speech = $this->speechModel->findById($data['speech']);

            foreach ($data['emails'] as $email){

                $result = $this->email
                    ->from('noreply@speechmanagement.com', "Speech Management")
                    ->to($email['value'])
                    ->subject($speech->subject)
                    ->message($speech->content)
                    ->send();
            }

            exit;
        }else{
            echo $this->unauthorizedResponse();
        }
    }

    public function updateSpeech($id){
        if($this->getBearerToken()) {
            $data = file_get_contents("php://input");
            $speech = json_decode($data, TRUE);
            print_r($this->speechModel->editSpeech($id, $speech));
        }else{
            echo $this->unauthorizedResponse();
        }

    }

    public function searchSpeech(){
        if($this->getBearerToken()) {
            echo json_encode($this->speechModel->findSpeech($_GET['author'], $_GET['subject'], $_GET['date']));
        }else{
            echo $this->unauthorizedResponse();
        }
    }

}