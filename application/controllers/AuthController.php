<?php
/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 1/7/2019
 * Time: 3:09 AM
 */

class AuthController extends MY_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function login(){
        $data = file_get_contents("php://input");
        $user = json_decode($data, TRUE);
        $res = $this->userModel->checkLogin($user['username'], $user['password']);

        $error = array(
            "status" => "error"
        );

        if($res != null) {
            if (password_verify($user['password'], $res->password)) {
                echo json_encode($res);
            } else {
                echo json_encode($error);
            }
        }else{
            echo json_encode($error);
        }

    }

    public function hashPassword(){
        $pass = $_GET['pass'];
        echo password_hash($pass, PASSWORD_DEFAULT);
    }

}